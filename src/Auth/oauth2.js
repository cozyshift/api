import oauth2orize from 'oauth2orize'
import Client from '../Client/model'
import User from '../User/model'
import Token from '../Token/model'
import Code from '../Code/model'
import uid from 'uid'

const server = oauth2orize.createServer()

server.serializeClient((client, cb) => cb(null, client._id))

server.deserializeClient((id, cb) => {
  Client.findOne({ _id: id }, (err, client) => {
    if (err) return cb(err)

    return cb(null, client)
  })
})

server.grant(oauth2orize.grant.code((client, redirectUri, user, ares, cb) => {
  const code = new Code({
    value: uid(16),
    clientId: client._id,
    redirectUri,
    userId: user._id,
  })

  code.save(err => {
    if (err) return cb(err)

    cb(null, code.value)
  })
}))

server.exchange(oauth2orize.exchange.password((client, username, password, scope, cb) => {
  User.findOne({ username }, '+password', (err, user) => {
    if (err) return cb(err)

    if (!user) return cb(null, false)

    user.verifyPassword(password, (error, isMatch) => {
      if (err) return cb(error)
      if (!isMatch) return cb(null, false)

      const token = new Token({
        value: uid(256),
        clientId: client._id,
        userId: user._id,
      })

      token.save(_error => {
        console.log(_error, client, typeof client)
        if (_error) return cb(_error)

        cb(null, token)
      })
    })
  })
}))

server.exchange(oauth2orize.exchange.code((client, code, redirectUri, cb) => {
  Code.findOne({ value: code }, (err, authCode) => {
    if (err) return cb(err)
    if (authCode === undefined) return cb(null, false)
    if (client._id.toString() !== authCode.clientId) return cb(null, false)
    if (redirectUri !== authCode.redirectUri) return cb(null, false)

    authCode.remove(error => {
      if (error) return cb(error)

      const token = new Token({
        value: uid(256),
        clientId: authCode.clientId,
        userId: authCode.userId,
      })

      token.save(_error => {
        if (_error) return cb(_error)

        cb(null, token)
      })
    })
  })
}))

// User authorization endpoint
const authorization = [
  server.authorization((clientId, redirectUri, cb) => {
    Client.findOne({ id: clientId }, (err, client) => {
      if (err) return cb(err)

      return cb(null, client, redirectUri)
    })
  }),
  (req, res) => {
    res.render('dialog', {
      transactionID: req.oauth2.transactionID,
      user: req.user,
      client: req.oauth2.client,
    })
  },
]

// User decision endpoint
const decision = [
  server.decision(),
]

// Application client token exchange endpoint
const token = [
  server.token(),
  server.errorHandler(),
]

export default {
  authorization,
  decision,
  token,
}
