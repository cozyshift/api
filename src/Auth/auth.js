import passport from 'passport'
import { BasicStrategy } from 'passport-http'
import { Strategy as BearerStrategy } from 'passport-http-bearer'
import User from '../User/model'
import Client from '../Client/model'
import Token from '../Token/model'

passport.use(new BasicStrategy((username, password, cb) => {
  User.findOne({ username }, (err, user) => {
    if (err) return cb(err)
    if (!user) return cb(null, false)
    user.verifyPassword(password, (error, isMatch) => {
      if (error) return cb(err)

      if (!isMatch) return cb(null, false)

      return cb(null, user)
    })
  })
}))

passport.use('client-basic', new BasicStrategy((username, password, cb) => {
  Client.findOne({ id: username }, (err, client) => {
    if (err) return cb(err)

    if (!client || client.secret !== password) return cb(null, false)

    return cb(null, client)
  })
}))

passport.use(new BearerStrategy((accessToken, cb) => {
  Token.findOne({ value: accessToken }, (err, token) => {
    if (err) return cb(err)

    if (!token) return cb(null, false)

    User.findOne({ _id: token.userId }, (error, user) => {
      if (error) return cb(error)

      if (!user) return cb(null, false)

      cb(null, user, { scope: '*' })
    })
  })
}))

export const isAuthenticated = passport.authenticate(['basic', 'bearer'], { session: false })
export const isClientAuthenticated = passport.authenticate('client-basic', { session: false })
