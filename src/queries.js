export default {
  ...require('./Company/query').default,
  ...require('./Employee/query').default,
  ...require('./Type/query').default,
  ...require('./Event/query').default,
  ...require('./User/query').default,
}
