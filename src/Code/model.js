import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  value: { type: String, required: true },
  redirectUri: { type: String, required: true },
  userId: { type: Schema.Types.ObjectId, required: true },
  clientId: { type: Schema.Types.ObjectId, required: true },
})

export default mongoose.model('Code', schema)
