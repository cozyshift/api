export default {
  ...require('./Company/mutation').default,
  ...require('./Employee/mutation').default,
  ...require('./Type/mutation').default,
  ...require('./Event/mutation').default,
  ...require('./User/mutation').default,
}
