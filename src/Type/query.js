import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLList,
} from 'graphql'

import { objectType } from './type'
import TypeModel from './model'
import getProjection from '../lib/projection'

import CompanyModel from '../Company/model'

export const single = {
  type: objectType,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: (root, args, options, ast) => {
    const projection = getProjection(ast.fieldASTs[0])
    function getType() {
      return TypeModel
        .findById(args.id)
        .select(projection)
        .exec()
    }

    if (root.user.isAdmin) {
      return getType()
    }

    return CompanyModel.findOne({
      userId: root.user._id,
      _id: args.companyId,
    }).exec().then(result => {
      if (!result) {
        throw new Error('Not permitted')
      }
    }).then(getType)
  },
}

export const multiple = {
  type: new GraphQLList(objectType),
  args: {
    companyId: {
      name: 'companyId',
      type: GraphQLID,
    },
  },
  resolve: (root, args, options, ast) => {
    const projection = getProjection(ast.fieldASTs[0])

    function getType(companies = []) {
      const companyIds = companies.map(company => company._id)
      return TypeModel
        .find({
          ...companies ? {
            companyId: { '$in': companyIds },
          } : {},
        })
        .select(projection)
        .exec()
    }

    if (root.user.isAdmin) {
      return getType()
    }

    return CompanyModel
      .find({
        userId: root.user._id,
        ...args.companyId ? {
          companyId: args.companyId,
        } : {},
      })
      .exec()
      .then(getType)
  },
}

export default {
  type: single,
  types: multiple,
}
