import {
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
} from 'graphql'

import { inputObjectType } from './type'
import TypeModel from './model'

import CompanyModel from '../Company/model'

export const add = {
  type: GraphQLBoolean,
  args: {
    data: {
      name: 'data',
      type: new GraphQLNonNull(inputObjectType),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    const company = await CompanyModel.findOne({
      _id: args.data.companyId,
      userId: rootValue.user._id,
    }).exec()

    if (!company && !rootValue.user.isAdmin) {
      throw new Error('Not permitted')
    }

    const typeModel = new TypeModel(args.data)

    const newType = await typeModel.save()

    if (!newType) {
      throw new Error('Error adding new Type')
    }

    return true
  },
}

export const remove = {
  type: GraphQLBoolean,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    const type = await TypeModel.findById(args._id).exec()

    const company = await CompanyModel.find({
      userId: rootValue.user._id,
      _id: type.companyId,
    }).exec()

    if (!company && !rootValue.user.isAdmin) {
      throw new Error('Not permitted')
    }

    const removeType = type.remove().exec()

    if (!removeType) {
      throw new Error('Error removing Employee')
    }

    return removeType
  },
}

export default {
  addType: add,
  removeType: remove,
}
