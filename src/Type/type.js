import {
  GraphQLInputObjectType,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLString,
} from 'graphql'

export const base = {
  name: 'Type',
  fields: {
    _id: {
      type: GraphQLID,
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    color: {
      type: new GraphQLNonNull(GraphQLString),
    },
    companyId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
}

export const objectType = new GraphQLObjectType(base)
export const inputObjectType = new GraphQLInputObjectType({
  ...base,
  name: 'TypeInput',
})

export default base
