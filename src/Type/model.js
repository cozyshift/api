import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  name: { type: String, required: true },
  color: { type: String, required: true },
  companyId: { type: Schema.Types.ObjectId, required: true },
})

export default mongoose.model('Type', schema)
