import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLList,
} from 'graphql'

import { objectType } from './type'
import UserModel from './model'

import getProjection from '../lib/projection'

export const single = {
  type: objectType,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])

    if (!rootValue.user.isAdmin && args._id !== rootValue.user._id) {
      throw new Error('Not Permitted')
    }

    return UserModel.findById(args._id).select(projection).exec()
  },
}

export const multiple = {
  type: new GraphQLList(objectType),
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])

    if (!rootValue.user.isAdmin) {
      throw new Error('Not Permitted')
    }

    return UserModel.find().select(projection).exec()
  },
}

export default {
  user: single,
  users: multiple,
}
