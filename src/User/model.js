import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcrypt'
import { isEmail } from 'validator'

const schema = new Schema({
  username: {
    type: String,
    lowercase: true,
    index: { unique: true, sparse: true },
    validate: {
      validator: value => isEmail(value),
      message: '{VALUE} is not a valid email!',
    },
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: false,
  },
})

schema.pre('save', function preSave(cb) {
  const _this = this

  if (!_this.isModified('password')) return cb()

  bcrypt.genSalt(5, (err, salt) => {
    if (err) return cb(err)

    bcrypt.hash(_this.password, salt, (error, hash) => {
      if (error) return cb(error)
      _this.password = hash
      cb()
    })
  })
})

// @todo get ride of context this and make it more es6
schema.methods.verifyPassword = function verifyPassword(password, cb) {
  bcrypt.compare(password, this.password, (err, isMatch) => {
    if (err) return cb(err)
    cb(null, isMatch)
  })
}

export default mongoose.model('User', schema)
