import {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLString,
} from 'graphql'

export const base = {
  name: 'User',
  fields: {
    _id: { type: new GraphQLNonNull(GraphQLID) },
    username: { type: new GraphQLNonNull(GraphQLString) },
    employeeId: { type: new GraphQLNonNull(GraphQLID) },
  },
}

export const objectType = new GraphQLObjectType(base)
export const inputObjectType = new GraphQLInputObjectType({
  ...base,
  name: 'UserInput',
  fields: {
    ...base.fields,
    password: { type: new GraphQLNonNull(GraphQLString) },
  },
})

export default base
