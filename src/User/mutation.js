import {
  GraphQLBoolean,
  GraphQLNonNull,
} from 'graphql'

import { inputObjectType } from './type'
import UserModel from './model'

export const add = {
  type: GraphQLBoolean,
  args: {
    data: {
      name: 'data',
      type: new GraphQLNonNull(inputObjectType),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    const userModel = new UserModel({
      ...args.data,
      isAdmin: false,
    })

    const newUser = await userModel.save()

    if (!newUser) {
      throw new Error('Error while registering')
    }

    return true
  },
}

export const remove = {
  type: GraphQLBoolean,
  args: {
    _id: {
      name: '_id',
      type: inputObjectType,
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    if (rootValue.user._id !== args._id) {
      throw new Error('Not Permitted')
    }

    return UserModel.findByIdAndRemove(args._id).exec()
  },
}

export default {
  addUser: add,
  removeUSer: remove,
}
