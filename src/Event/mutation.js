import {
  GraphQLID,
  GraphQLBoolean,
  GraphQLNonNull,
} from 'graphql'

import { inputObjectType } from './type'
import EventModel from './model'
import CompanyModel from '../Company/model'
import EmployeeModel from '../Employee/model'

function getEmployee(employeeId, user) {
  return CompanyModel.find({
    userId: user._id,
  })
  .select('_id')
  .exec()
  .then(companies => companies.map(company => company.id))
  .then(companyIds => {
    return EmployeeModel.findOne({
      _id: employeeId,
      companyId: { $in: companyIds },
    }).select('_id').exec()
  }).then(employee => {
    return employee
  })
}

export const add = {
  type: GraphQLBoolean,
  args: {
    data: {
      name: 'data',
      type: new GraphQLNonNull(inputObjectType),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    if (!rootValue.user.isAdmin) {
      const employee = await getEmployee(args.data.employeeId, rootValue.user)

      if (!employee) {
        throw new Error('Not Permitted')
      }
    }

    const eventModel = new EventModel({
      ...args.data,
      start: new Date(args.data.start),
      end: new Date(args.data.end),
    })

    const newEvent = await eventModel.save()

    if (!newEvent) {
      throw new Error('Error adding new Event')
    }

    return true
  },
}

export const remove = {
  type: GraphQLBoolean,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    if (!rootValue.user.isAdmin) {
      const employee = await getEmployee(args._id, rootValue.user)
      if (!employee) {
        throw new Error('Not permitted')
      }
    }

    return EmployeeModel.findByIdAndRemove(args._id).exec()
  },
}

export default {
  addEvent: add,
  removeEvent: remove,
}
