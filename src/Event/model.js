import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  start: { type: Date, required: true },
  end: { type: Date, required: true },
})

export default mongoose.model('Event', schema)
