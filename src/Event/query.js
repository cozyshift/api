import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLList,
  GraphQLString,
} from 'graphql'

import { objectType } from './type'
import EventModel from './model'
import getProjection from '../lib/projection'

import CompanyModel from '../Company/model'
import EmployeeModel from '../Employee/model'

export const single = {
  type: objectType,
  args: {
    id: {
      name: 'id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])
    function getEvent(employees) {
      const employeeIds = employees ? employees.map(employee => employee._id) : []

      return EventModel
        .findOne({
          _id: args.id,
          ...employees ? {
            employeeId: { $in: employeeIds },
          } : {},
        })
        .select(projection)
        .exec()
    }

    if (rootValue.user.isAdmin) {
      return getEvent()
    }

    function getEmployees(companies = []) {
      const companyIds = companies.map(company => company._id)
      return EmployeeModel.find({
        userId: rootValue.user._id,
        companyId: { $in: companyIds },
      }).select('_id').exec()
    }

    function getCompanies() {
      return CompanyModel.find({
        userId: rootValue.user._id,
      }).select('_id').exec()
    }

    return getCompanies().then(getEmployees).then(getEvent)
  },
}

export const multiple = {
  args: {
    companyId: {
      name: 'companyId',
      type: GraphQLID,
    },
    employeeId: {
      name: 'companyId',
      type: GraphQLID,
    },
    start: {
      name: 'start',
      type: new GraphQLNonNull(GraphQLString),
    },
    end: {
      name: 'end',
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  type: new GraphQLList(objectType),
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])
    function getEvents(employees) {
      const employeeIds = employees ? employees.map(employee => employee._id) : []
      return EventModel.find({
        ...employees ? {
          employeeId: { $in: employeeIds },
        } : {},
        start: {
          $gte: new Date(args.start),
          $lt: new Date(args.end),
        },
        end: {
          $gt: new Date(args.start),
          $lte: new Date(args.end),
        },
      })
      .select(projection)
      .exec()
    }

    if (rootValue.user.isAdmin) {
      return getEvents()
    }

    function getEmployees(companies = []) {
      const companyIds = companies.map(company => company._id)
      return EmployeeModel.find({
        ...args.employeeId ? {
          _id: args.employeeId,
        } : {},
        userId: rootValue.user._id,
        companyId: { $in: companyIds },
      }).select('_id').exec()
    }

    function getCompanies() {
      return CompanyModel.find({
        ...args.companyId ? {
          _id: args.companyId,
        } : {},
        userId: rootValue.user._id,
      }).select('_id').exec()
    }

    return getCompanies().then(getEmployees).then(getEvents)
  },
}

export default {
  event: single,
  events: multiple,
}
