import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
  GraphQLObjectType,
  GraphQLInputObjectType,
} from 'graphql'

import EventModel from './model'
import EmployeeModel from '../Employee/model'
import { objectType as EmployeeType } from '../Employee/type'
import getProjection from '../lib/projection'

export const base = {
  name: 'Event',
  fields: {
    _id: { type: new GraphQLNonNull(GraphQLID) },
    employeeId: { type: new GraphQLNonNull(GraphQLID) },
    start: { type: new GraphQLNonNull(GraphQLString) },
    end: { type: new GraphQLNonNull(GraphQLString) },
  },
}

export const objectType = new GraphQLObjectType({
  ...base,
  fields: () => ({
    ...base.fields,
    employee: {
      type: EmployeeType,
      resolve: (source, args, context, {fieldASTs, rootValue}) => {
        const projection = getProjection(fieldASTs[0])
        if (!source.employeeId) {
          return EventModel.findById(source._id).select('employeeId').exec().then(
            ({employeeId}) => EmployeeModel.findById(employeeId).select(projection).exec()
          )
        }
        return EmployeeModel.findById(source.employeeId).select(projection).exec()
      },
    },
  }),
})
export const inputObjectType = new GraphQLInputObjectType({
  ...base,
  name: 'EventInput',
})

export default base
