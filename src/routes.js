import { Router as router } from 'express'
import { isAuthenticated, isClientAuthenticated } from './Auth/auth'
import oauth2 from './Auth/oauth2'

const routes = router()

routes.get('/', (req, res) => {
  res.json({ status: 'running' })
})

// Create endpoint handlers for oauth2 authorize
routes.route('/oauth2/authorize')
  .get(isAuthenticated, oauth2.authorization)
  .post(isAuthenticated, oauth2.decision)

// Create endpoint handlers for oauth2 token
routes.route('/oauth2/token')
  .post(isClientAuthenticated, oauth2.token)

export default routes
