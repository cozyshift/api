import {
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
} from 'graphql'

import { inputObjectType } from './type'
import EmployeeModel from './model'

import CompanyModel from '../Company/model'

export const add = {
  type: GraphQLBoolean,
  args: {
    data: {
      name: 'data',
      type: new GraphQLNonNull(inputObjectType),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    const company = await CompanyModel.findOne({
      _id: args.data.companyId,
      userId: rootValue.user._id,
    }).exec()

    if (!company && !rootValue.user.isAdmin) {
      throw new Error('Not permitted')
    }

    const employeeModel = new EmployeeModel(args.data)

    const newEmployee = await employeeModel.save()

    if (!newEmployee) {
      throw new Error('Error adding new Employee')
    }

    return true
  },
}

export const remove = {
  type: GraphQLBoolean,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  async resolve(source, args, context, {fieldASTs, rootValue}) {
    const employee = await EmployeeModel.findById(args._id).exec()

    const company = await CompanyModel.find({
      userId: rootValue.user._id,
      _id: employee.companyId,
    }).exec()

    if (!company && !rootValue.user.isAdmin) {
      throw new Error('Not permitted')
    }

    const removeEmployee = employee.remove().exec()

    if (!removeEmployee) {
      throw new Error('Error removing Employee')
    }

    return removeEmployee
  },
}

export default {
  addEmployee: add,
  removeEmployee: remove,
}
