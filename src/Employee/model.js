import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: {
    type: String,
    required: true,
    lowercase: true,
    index: { unique: true, sparse: true },
  },
  companyId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  typeId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
})

export default mongoose.model('Employee', schema)
