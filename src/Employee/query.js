import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLList,
  GraphQLString,
} from 'graphql'

import { objectType } from './type'
import employeeModel from './model'
import getProjection from '../lib/projection'

import CompanyModel from '../Company/model'

export const single = {
  type: objectType,
  args: {
    id: {
      name: 'id',
      type: new GraphQLNonNull(GraphQLID),
    },
    email: {
      name: 'email',
      type: GraphQLString,
    },
  },
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])
    function getEmployee() {
      return employeeModel
        .findById(args.id)
        .select(projection)
        .exec()
    }

    if (rootValue.user.isAdmin) {
      return getEmployee()
    }

    return CompanyModel.findOne({
      userId: rootValue.user._id,
      _id: args.id,
    }).exec().then(result => {
      if (!result) {
        throw new Error('Not permitted')
      }
    }).then(getEmployee)
  },
}

export const multiple = {
  args: {
    companyId: {
      name: 'companyId',
      type: GraphQLID,
    },
  },
  type: new GraphQLList(objectType),
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])

    function getEmployee(companyIds) {
      return employeeModel
        .find({
          companyId: { $in: companyIds },
        })
        .select(projection)
        .exec()
    }
    return CompanyModel
      .find({
        ...!rootValue.user.isAdmin ? {
          userId: rootValue.user._id,
        } : {},
        ...args.companyId ? {
          _id: args.companyId,
        } : {},
      })
      .select('_id')
      .exec()
      .then(result => {
        if (!result) {
          throw new Error('Not permitted')
        }

        return result.map(company => company._id)
      })
      .then(getEmployee)
  },
}

export default {
  employee: single,
  employees: multiple,
}
