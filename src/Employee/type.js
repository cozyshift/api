import {
  GraphQLInputObjectType,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
  GraphQLList,
} from 'graphql'

import getProjection from '../lib/projection'

import { objectType as typeType } from '../Type/type'
import TypeModel from '../Type/model'
import EmployeeModel from './model'
import EventModel from '../Event/model'
import { objectType as EventType } from '../Event/type'

export const base = {
  name: 'Employee',
  fields: {
    _id: {
      type: GraphQLID,
    },
    firstname: {
      type: GraphQLString,
    },
    lastname: {
      type: GraphQLString,
    },
    email: {
      type: GraphQLString,
    },
    companyId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    typeId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
}

export const objectType = new GraphQLObjectType({
  ...base,
  fields: () => ({
    ...base.fields,
    type: {
      type: typeType,
      resolve: (source, args, context, {fieldASTs, rootValue}) => {
        const projection = getProjection(fieldASTs[0])
        if (!source.typeId) {
          return EmployeeModel.findById(source._id).select('typeId').exec().then(({ typeId }) =>
            TypeModel.findById(typeId).select(projection).exec()
          )
        }
        return TypeModel.findById(source.typeId).select(projection).exec()
      },
    },
    events: {
      type: new GraphQLList(EventType),
      args: {
        start: { type: GraphQLString },
        end: { type: GraphQLString },
      },
      resolve: (source, args, context, {fieldASTs, rootValue}) => {
        const projection = getProjection(fieldASTs[0])
        const {start, end} = args
        return EventModel.find({
          employeeId: source._id,
          ...start ? {
            start: {
              $gte: new Date(args.start),
              $lt: new Date(args.end),
            },
          } : {},
          ...end ? {
            end: {
              $gt: new Date(args.start),
              $lte: new Date(args.end),
            },
          } : {},
        }).select(projection).exec()
      },
    },
  }),
})

export const inputObjectType = new GraphQLInputObjectType({
  ...base,
  name: 'EmployeeInput',
})
export default base
