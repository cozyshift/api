import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  name: { type: String, required: true },
  userId: { type: Schema.Types.ObjectId, required: true },
})

export default mongoose.model('Company', schema)
