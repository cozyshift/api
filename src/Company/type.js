import {
  GraphQLList,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
} from 'graphql'

import getProjection from '../lib/projection'
import { objectType as employeeType } from '../Employee/type'
import EmployeeModel from '../Employee/model'

import { objectType as typeType } from '../Type/type'
import TypeModel from '../Type/model'

export const base = {
  name: 'Company',
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    userId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
}

export const objectType = new GraphQLObjectType({
  ...base,
  fields: {
    ...base.fields,
    employees: {
      type: new GraphQLList(employeeType),
      resolve: (source, args, context, {fieldASTs, rootValue}) => {
        const projection = getProjection(fieldASTs[0])

        return EmployeeModel.find({
          companyId: source._id,
        }).select(projection).exec()
      },
    },
    types: {
      type: new GraphQLList(typeType),
      resolve: (source, args, context, {fieldASTs, rootValue}) => {
        const projection = getProjection(fieldASTs[0])

        return TypeModel.find({
          companyId: source._id,
        }).select(projection).exec()
      },
    },
  },
})

export const inputObjectType = new GraphQLInputObjectType({
  ...base,
  name: 'CompanyInput',
})

export default base
