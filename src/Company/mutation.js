import {
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
} from 'graphql'

import { inputObjectType } from './type'
import CompanyModel from './model'

export const add = {
  type: GraphQLBoolean,
  args: {
    data: {
      name: 'data',
      type: new GraphQLNonNull(inputObjectType),
    },
  },
  async resolve(root, params, options) {
    const company = new CompanyModel({
      ...params.data,
      userId: root.user._id,
    })

    const newCompany = await company.save()
    if (!newCompany) {
      throw new Error('Error adding new Company')
    }

    return true
  },
}

export const remove = {
  type: GraphQLBoolean,
  args: {
    _id: {
      name: '_id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  async resolve(root, params, options) {
    const removeEmployee = await CompanyModel
      .findOneAndRemove({
        id: params._id,
        userId: root.user._id,
      })
      .exec()

    if (!removeEmployee) {
      throw new Error('Error removing Employee')
    }

    return removeEmployee
  },
}

export default {
  addCompany: add,
  removeCompany: remove,
}
