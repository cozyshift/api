import {
  GraphQLNonNull,
  GraphQLID,
  GraphQLList,
} from 'graphql'

import { objectType } from './type'
import companyModel from './model'
import getProjection from '../lib/projection'

export const single = {
  type: objectType,
  args: {
    id: {
      name: 'id',
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])

    return companyModel
      .find({
        _id: args.id,
        ...!rootValue.user.isAdmin ? {
          userId: rootValue.user._id,
        } : {},
      })
      .select(projection)
      .exec()
  },
}

export const multiple = {
  type: new GraphQLList(objectType),
  resolve: (source, args, context, {fieldASTs, rootValue}) => {
    const projection = getProjection(fieldASTs[0])
    return companyModel.find({
      ...!rootValue.user.isAdmin ? {
        userId: rootValue.user._id,
      } : {},
    }).select(projection).exec()
  },
}

export default {
  company: single,
  companies: multiple,
}
