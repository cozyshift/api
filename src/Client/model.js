import mongoose, { Schema } from 'mongoose'

const schema = new Schema({
  name: { type: String, unique: true, require: true },
  id: { type: String, require: true },
  secret: { type: String, require: true },
  userId: { type: Schema.Types.ObjectId, require: true },
  authorizedUri: { type: Array, default: [] },
})

export default mongoose.model('Client', schema)
