// @todo See if refactor
import _ from 'lodash'

export function errorFormatter(param, msg, value, extra) {
  return {
    path: param,
    message: msg,
    value,
    extra: extra || { },
  }
}

export function mongooseErrorFormatter(error) {
  return Object.assign(errorFormatter(error.path, error.message, error.value || null), {
    type: error.kind,
    name: error.name,
  })
}

function fail(errors) {
  let _errors = errors

  if (_.isArray(_errors)) {
    _errors = _.groupBy(errors, 'path')
  }

  return {
    status: 'fail',
    data: _errors,
  }
}

function success(data) {
  return {
    status: 'success',
    data,
  }
}

export default {
  fail,
  success,
}
