if (!process.env.MONGO_PORT_27017_TCP_ADDR || !process.env.MONGO_PORT_27017_TCP_PORT) {
  let error = 'You need to specify mongo environement variables: '
  error += 'MONGO_PORT_27017_TCP_ADDR, MONGO_PORT_27017_TCP_PORT'
  throw new Error(error)
}

const main = {
  URL: `mongodb://${process.env.MONGO_PORT_27017_TCP_ADDR}:${process.env.MONGO_PORT_27017_TCP_PORT}/cozyshift`,
}

export { main as default }
