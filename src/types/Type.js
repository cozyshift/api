import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  color: { type: String, required: true },
  companyId: { type: String, required: true },
})

export default mongoose.model('Type', schema)
