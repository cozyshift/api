import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
} from 'graphql'

export default new GraphQLObjectType({
  name: 'Employee',
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    firstname: {
      type: GraphQLString,
    },
    lastname: {
      type: GraphQLString,
    },
    email: {
      type: GraphQLString,
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    typeId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
})
