import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
} from 'graphql'

export default new GraphQLObjectType({
  name: 'Event',
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    start: {
      type: new GraphQLNonNull(GraphQLString),
    },
    end: {
      type: new GraphQLNonNull(GraphQLString),
    },
    employeeId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
})
