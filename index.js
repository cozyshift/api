import express from 'express'
import bodyParser from 'body-parser'
import routes from './src/routes'
import secret from './src/config/secret'
import cookieParser from 'cookie-parser'
import mongoose from 'mongoose'
import database from './src/config/database'
import passport from 'passport'
import session from 'express-session'
import morgan from 'morgan'
import cors from 'cors'
import graphqlHTTP from 'express-graphql'
import { isAuthenticated, isClientAuthenticated } from './src/Auth/auth'
import { add as addUser } from './src/User/mutation'
import { single as employee } from './src/Employee/query'
import {
  GraphQLSchema,
  GraphQLObjectType,
} from 'graphql'

import schema from './src/schema'

export const port = 9000

const server = global.server = express()

mongoose.Promise = global.Promise

// Add session handler
server.use(session({
  secret,
  saveUninitialized: true,
  resave: true,
}))
server.use(cors())
server.use(morgan('dev'))
server.use(bodyParser.json())
server.set('view engine', 'ejs')
server.set('views', `${process.cwd()}/api/views`)
server.use(cookieParser(secret))
server.use(passport.initialize())
server.use(routes)
server.use('/graphql', isAuthenticated, graphqlHTTP((req, res) => ({
  schema,
  pretty: process.env.NODE_ENV !== 'production',
  graphiql: process.env.NODE_ENV !== 'production',
  rootValue: { user: req.user },
})))
server.use('/register', isClientAuthenticated, graphqlHTTP((req, res) => {
  return {
    schema: new GraphQLSchema({
      mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
          addUser,
        },
      }),
      query: new GraphQLObjectType({
        name: 'Query',
        fields: {
          employee,
        },
      }),
    }),
    pretty: process.env.NODE_ENV !== 'production',
    graphiql: process.env.NODE_ENV !== 'production',
  }
}))

mongoose.connect(database.URL)

// Launch the server
// -----------------------------------------------------------------------------
server.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`The server is running at http://localhost:${port}/`)
})

export default server
