import nodemon from 'nodemon'

async function start() {
  nodemon({
    script: 'index.js',
    ext: 'js json ejs',
    exec: 'npm run babel',
  })

  nodemon.on('start', function() {
    console.log('App has started')
  }).on('quit', function() {
    console.log('App has quit')
  }).on('restart', function(files) {
    console.log('App restarted due to: ', files)
  })

  process.on('SIGINT', () => {
    process.exit()
  })
}

export default start
