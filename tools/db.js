import mongoose from 'mongoose'
import cmd from 'commander'
import database from '../src/database'

async function initDb() {
  cmd.version('0.0.1')
    .option('-f', '--force', 'Reinitialize existant database')

  if (cmd.force) {
    mongoose.connect(database.URL, () => {
      mongoose.connection.db.dropDatabase()
    })
  }
}

export default initDb
